# INTRODUCTION

Aggregation Per Role is a module which allows users to debug CSS and Js
with their role without affecting other role's performance.

By default it disables aggregation to the administrator role, but more 
roles can be configured to toggle off this performance setting.

## INSTALLATION:

  * Installation is like all normal drupal modules:
   * Extract the 'aggregation_per_role' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).
   * Go to /admin/modules and enable the 'aggregation_per_role' module

## REQUIREMENTS:

  * The Aggregation Per Role module has no dependencies to other modules.

## CONFIGURATION

  * Enable or disable the functionality with a toggle.
  * Specify the roles which should have aggregation disabled in a text area.

## Conflicts/known issues:

  * There are no current conflicts nor known issues with this module.
