<?php

/**
 * @file
 * Aggregation Per Role configuration.
 */

/**
 * Custom form for module configuration.
 */
function aggregation_per_role_admin_form($form, &$form_state) {

  // Honeypot Configuration.
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['general']['aggregation_per_role__enable_functionality'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable functionality'),
    '#description' => t('Activate the aggregation per role functionality.<br\>By default the aggregation is disabled for administrator users.'),
    '#default_value' => variable_get('aggregation_per_role__enable_functionality', 1),
  );
  $form['general']['aggregation_per_role__css'] = array(
    '#type' => 'checkbox',
    '#title' => t('CSS'),
    '#description' => t('Disable aggregation for CSS.'),
    '#default_value' => variable_get('aggregation_per_role__css', 1),
  );
  $form['general']['aggregation_per_role__js'] = array(
    '#type' => 'checkbox',
    '#title' => t('JS'),
    '#description' => t('Disable aggregation for JS.'),
    '#default_value' => variable_get('aggregation_per_role__js', 1),
  );
  $form['general']['aggregation_per_role__roles'] = array(
    '#type' => 'textarea',
    '#title' => t('Roles'),
    '#description' => t('Enter a list of roles (separated by newlines) which should be considered for aggregation removal.<br/>By entering something here, you override the default role: administrator.'),
    '#default_value' => variable_get('aggregation_per_role__roles', ''),
  );
  return system_settings_form($form);
}
